//
//  ViewController.swift
//  ios12c01
//
//  Created by Admin on 4/22/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       // setupPg12()
       // setupPg14()
        //setupPg14b()
       // setupPg15()
        //setupPg14c()
        //pg17()
       // pg18()
       // pg19()
        //pg19b()
        //pg19c()
        //pg20()
        //pg21()
        //pg28()
        //pg32()
        //pg33()
        pg40()
        
        
    }
    
    var v2 : UIView! // we retain this for future use
    var constraintsWith = [NSLayoutConstraint]()
    var constraintsWithout = [NSLayoutConstraint]()
    
    func pg40() {
        print("Add and remove constraits as we add/remove a view")
        let v1 = UIView()
        v1.backgroundColor = .red
        v1.translatesAutoresizingMaskIntoConstraints = false
        let v2 = UIView()
        v2.backgroundColor = .yellow
        v2.translatesAutoresizingMaskIntoConstraints = false
        let v3 = UIView()
        v3.backgroundColor = .blue
        v3.translatesAutoresizingMaskIntoConstraints = false
        // 3 views added to main view
        self.view.addSubview(v1)
        self.view.addSubview(v2)
        self.view.addSubview(v3)
        
        self.v2 = v2 // retain
        let c1 = NSLayoutConstraint.constraints(withVisualFormat:
            "H:|-(20)-[v(100)]", metrics: nil, views: ["v":v1])
        // c2 is for V2
        let c2 = NSLayoutConstraint.constraints(withVisualFormat:
            "H:|-(20)-[v(100)]", metrics: nil, views: ["v":v2])
        let c3 = NSLayoutConstraint.constraints(withVisualFormat:
            "H:|-(20)-[v(100)]", metrics: nil, views: ["v":v3])
        let c4 = NSLayoutConstraint.constraints(withVisualFormat:
            "V:|-(100)-[v(20)]", metrics: nil, views: ["v":v1])
        // if v2 present:
        // | v1
        // | v2
        // | v3
        // vertial with v2
        let c5with = NSLayoutConstraint.constraints(withVisualFormat:
            "V:[v1]-(20)-[v2(20)]-(20)-[v3(20)]", metrics: nil,
                                                  views: ["v1":v1, "v2":v2, "v3":v3])
        //if v2 removed
        // |-------------------
        // | v1
        // | v3
        // ------------------
        // vertical without v2
        let c5without = NSLayoutConstraint.constraints(withVisualFormat:
            
            "V:[v1]-(20)-[v3(20)]", metrics: nil, views: ["v1":v1, "v3":v3])
        // APPLY COMMON CONSTRAINTS  !!!!
        NSLayoutConstraint.activate([c1, c3, c4].flatMap{$0})
        
        // first set of constraints (for when v2 is present)
        self.constraintsWith.append(contentsOf:c2)
        self.constraintsWith.append(contentsOf:c5with) //with  c2 & c5 with
        
        // second set of constraints (for when v2 is absent)
        self.constraintsWithout.append(contentsOf:c5without) // c5withoutonly
        
        // apply first set
        NSLayoutConstraint.activate(self.constraintsWith)
        
        // v2 is retained from super view. It will be retained in memory
        // because we hold a reference to it.
        // alternate constraints will kick in
        
        if self.v2.superview != nil {
            self.v2.removeFromSuperview()
            NSLayoutConstraint.deactivate(self.constraintsWith)
            NSLayoutConstraint.activate(self.constraintsWithout)
        } else {
            //remove it, there will be only 2 bars
            //deactivate the old constraints, activate the new ones
            self.view.addSubview(v2)
            NSLayoutConstraint.deactivate(self.constraintsWithout)
            NSLayoutConstraint.activate(self.constraintsWith)
        }
    }
    
    func pg33() {
        
        let v1 = UIView(frame:CGRect(100, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView()
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        let v3 = UIView()
        v3.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        v1.addSubview(v3)
        v2.translatesAutoresizingMaskIntoConstraints = false
        v3.translatesAutoresizingMaskIntoConstraints = false
        
        v1.addConstraint(
            NSLayoutConstraint(item: v2,
                               attribute: .leading, //left margin
                               relatedBy: .equal,
                               toItem: v1,
                               attribute: .leading,
                               multiplier: 1, constant: 0)
        )
        v1.addConstraint(
            NSLayoutConstraint(item: v2,
                               attribute: .trailing, //right
                               relatedBy: .equal,
                               toItem: v1,
                               attribute: .trailing,
                               multiplier: 1, constant: 0)
        )
        v1.addConstraint(
            NSLayoutConstraint(item: v2,
                               attribute: .top,  //top
                               relatedBy: .equal,
                               toItem: v1,
                               attribute: .top,
                               multiplier: 1, constant: 0)
        )
        v2.addConstraint(
            NSLayoutConstraint(item: v2,
                               attribute: .height,  //height
                               
                               relatedBy: .equal,
                               toItem: nil,
                               attribute: .notAnAttribute,
                               multiplier: 1, constant: 10)
        )
        v3.addConstraint(
            NSLayoutConstraint(item: v3,
                               attribute: .width,
                               relatedBy: .equal,
                               toItem: nil,
                               attribute: .notAnAttribute,
                               multiplier: 1, constant: 20)
        )
        v3.addConstraint(
            NSLayoutConstraint(item: v3,
                               attribute: .height,
                               relatedBy: .equal,
                               toItem: nil,
                               attribute: .notAnAttribute,
                               multiplier: 1, constant: 20)
        )
        v1.addConstraint(
            NSLayoutConstraint(item: v3,
                               attribute: .trailing,
                               relatedBy: .equal,
                               toItem: v1,
                               attribute: .trailing,
                               multiplier: 1, constant: 0)
        )
        
        v1.addConstraint(NSLayoutConstraint(item: v3,
                                            attribute: .bottom,
                                            relatedBy: .equal,
                                            toItem: v1,
                                            attribute: .bottom,
                                            multiplier: 1,
                                            constant: 0))
        
    }
    func pg32() {
        print("Adding label with AL referencing a label positioned with AR does work, UNTIL YOU ADD MORE CONSTRAINTS TO V1 ")
        let lab1 = UILabel(frame:CGRect(270,30,42,22))
        lab1.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        lab1.text = "Hello"
        lab1.backgroundColor = .red
        self.view.addSubview(lab1)
        
        let lab2 = UILabel()
        lab2.backgroundColor = .green
        lab2.translatesAutoresizingMaskIntoConstraints = false
        lab2.text = "Howdy"
        
        self.view.addSubview(lab2)
        
        NSLayoutConstraint.activate([
            lab2.topAnchor.constraint(
                equalTo: lab1.bottomAnchor, constant: 20),
            lab2.trailingAnchor.constraint(
                equalTo: self.view.trailingAnchor, constant: -20)
            ])
        
        
        
        
        
    }
    func pg28() {
        print("example using springs/struts ")
        let v1 = UIView(frame:CGRect(100, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        
        let v2 = UIView(frame:CGRect(0, 0, 132, 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        
        v2.autoresizingMask = .flexibleWidth //
        
        let v1b = v1.bounds
        //bottom right corner
        //top left corner is 20 from right, 20 up from bottom, 20 pt sq
        let v3 = UIView(frame:CGRect(v1b.width-20, v1b.height-20, 20, 20))
        v3.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        
        //bottom corner
        v3.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin] //bottom RT corner square
        // top spring, left spring, bottom fixed, rt fixed
        
        self.view.addSubview(v1)

        
        v1.addSubview(v2)  //bar across top of v1
        v1.addSubview(v3)
        
        
        v1.bounds.size.width += 90
        v1.bounds.size.height += 90
    
        
        
        
        
    }
    func pg21() {
        print("Skew")
        let v1 = UIView(frame: CGRect(113, 111, 132, 94))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        v1.transform = CGAffineTransform(a: 1, b: 0, c: -0.2, d: 1, tx: 0, ty: 00)
    }
    func pg20() {
        print("rotation then translation")
        let v1 = UIView(frame: CGRect(113, 111, 132, 94))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        // same height, larger x
        v1.transform = CGAffineTransform(rotationAngle: 45 * .pi/180).translatedBy(x: 100, y: 0)
       // v1.transform = CGAffineTransform.identity  //revert it back to original
        
    }
    func pg19c() {
        print("Translation then rotation ")
        let v1 = UIView(frame: CGRect(113, 111, 132, 94))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        // same height, larger x
        v1.transform = CGAffineTransform(translationX: 100, y: 0).rotated(by: 45 * .pi/180)
        
        
    }
    func pg19b() {
        //translate to right, then rotate 45
        print("Apply a scale transform ")
        let v1 = UIView(frame: CGRect(113, 111, 132, 94))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        // same height, larger x
        v1.transform = CGAffineTransform(translationX: 100, y: 0)
        
    }
    func pg19() {
        print("Apply a scale transform ")
        let v1 = UIView(frame: CGRect(113, 111, 132, 94))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        // same height, larger x
        v1.transform = CGAffineTransform(scaleX: 1.8, y: 1)
        
        
    }
    func pg18() {
        print("Transform by 45 degree clockwise rotation")
        let v1 = UIView(frame: CGRect(113, 111, 132, 94))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        v1.transform = CGAffineTransform(rotationAngle: 45 * .pi/180)
        
    }
    func pg17() {
        let v1 = UIView(frame:CGRect(113, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        
        let v2 = UIView(frame: CGRect(30, 70, 10, 10)) //bounds of the superview in its own coordinates
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        
        self.view.addSubview(v1)
        v1.addSubview(v2)
        
        //now center it
        
       // v2.center = CGPoint(x:v1.bounds.midX, y:v1.bounds.midY)
        
        // or convert the coordinate systems
        v2.center =  v1.convert(v1.center, from: v1.superview )
        
        
        // From Hacking with Swift
        
        let view1 = UIView(frame: CGRect(x: 50, y: 50, width: 128, height: 128))
        let view2 = UIView(frame: CGRect(x: 200, y: 200, width: 128, height: 128))
        
        let tap = CGPoint(x: 150, y: 150) // will be 0,0 in terms of v2's origin
        let convertedTap = view1.convert(tap, to: view2)
        print("converted tap ", convertedTap)
        
    }
    func setupPg14c() {
        
        let v1 = UIView(frame: CGRect(113, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame: v1.bounds.insetBy(dx: 50, dy: 50))
        v2.backgroundColor = UIColor(red: 0.5, green: 1.0, blue: 0, alpha:  1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        print(v2.bounds)
        print(v2.frame)
        let cbefore = CGPoint(x: v2.bounds.midX, y: v2.bounds.midY)
        
       // v2.bounds.size.height += 0
        v2.bounds.size.width += 200
        let cafter = CGPoint(x: v2.bounds.midX, y: v2.bounds.midY)
        print()
        
        print(v2.bounds)
        print(v2.frame)
        print()
        print(cbefore)
        print(cafter)
    }
    func setupPg15() {
        let v1 = UIView(frame:CGRect(113, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame:v1.bounds.insetBy(dx: 10, dy: 10))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
        print(v2.bounds)
        print(v2.frame)
        
        // the subview always draws its rectangle from where it thinks 0,0 is.
        // if origin is 10,10... then 0,0 must be further left, up 
       // v1.bounds.origin.x += 10
       // v1.bounds.origin.y += 10
        print(v2.bounds)
        print(v2.frame)
        
        
    }
    func setupPg14() {
        let v1 = UIView(frame:CGRect(113, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        
        let v2 = UIView(frame:v1.bounds.insetBy(dx: 10, dy: 10)) //bounds of the superview in its own coordinates
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        
        self.view.addSubview(v1)
        v1.addSubview(v2)
    }
    func setupPg12() {
        let v1 = UIView(frame: CGRect( 113, 111, 132, 194))
        
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        let v2 = UIView(frame:CGRect(41, 56, 132, 194))
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        let v3 = UIView(frame:CGRect(43, 197, 160, 230))
        v3.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        self.view.addSubview(v1) //added to view
        v1.addSubview(v2)   //added to v1
        self.view.addSubview(v3) //added to view
        self.view.addSubview(v1)
    }
    
    func setupPg14b() {
        
        let v1 = UIView(frame:CGRect(113, 111, 132, 194))
        v1.backgroundColor = UIColor(red: 1, green: 0.4, blue: 1, alpha: 1)
        print("origin", v1.bounds.origin,"h", v1.bounds.height, "w", v1.bounds.width)
        let v2 = UIView(frame:v1.bounds.insetBy(dx: 10, dy: 10))
       // let v2 = UIView(frame: v1.bounds)
        v2.backgroundColor = UIColor(red: 0.5, green: 1, blue: 0, alpha: 1)
        self.view.addSubview(v1)
        v1.addSubview(v2)
       v2.bounds.size.height += 20
       v2.bounds.size.width += 20
        
    }


}

