//
//  MNExtensons.swift
//  ios12c01
//
//  Created by Admin on 4/22/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

extension CGRect {
    
    init(_ x:CGFloat, _ y:CGFloat, _ w:CGFloat, _ h:CGFloat) {
        self.init(x:x, y:y, width:w, height:h)
    }
    
}

